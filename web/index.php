<?php
$loader = require_once __DIR__ . '/../vendor/autoload.php';

define( 'PATH_WEB', __DIR__ );
define( 'PATH_ROOT', realpath(PATH_WEB . '/..') );
define( 'PATH_CACHE', PATH_ROOT . '/var/cache' );
define( 'PATH_LOG', PATH_ROOT . '/var/log' );

define( 'PATH_VENDOR', PATH_ROOT . '/vendor' );
define( 'PATH_APP', PATH_ROOT . '/app' );
define( 'PATH_LOCAL', PATH_APP . '/resource/localisation' );
define( 'PATH_TEMPLATES', PATH_APP . '/resource/template' );

use Symfony\Component\Debug\Debug;
use tfeiszt\silex\Application;
use tfeiszt\helper\Helper;

define( 'ENVIRONMENT',  getenv('SYMFONY_ENV') ?: 'production' ); //dev, staging, production

Helper::setEnv(ENVIRONMENT);

use Doctrine\Common\Annotations\AnnotationRegistry;
AnnotationRegistry::registerLoader([$loader, 'loadClass']);

$app = new Application();

require PATH_ROOT . '/config/' . ENVIRONMENT . '/config.php';

require PATH_ROOT . '/config/app.php';

require PATH_ROOT . '/config/routes.php';

use Symfony\Component\HttpFoundation\Request;
Request::setTrustedProxies(array('127.0.0.1'));
if ($app['debug'] === true || ENVIRONMENT == 'dev' ) {
// http cache is not allowed
    $app->run();
} else {
    $app['http_cache']->run();
}
