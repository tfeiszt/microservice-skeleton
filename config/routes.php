<?php

use Symfony\Component\HttpFoundation\Request;

/**
 * Defining exception output
 */
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    if ($request->headers->has('content-type') && strtolower($request->headers->get('content-type')) == 'application/json') {
        return $app['output.json']($e);
    }
    return $app['output.render']('', $e);
});

/**
 * Calculating controller class
 */
$segment = $app->getControllerSegment(Request::createFromGlobals());
$className = $app->getControllerClassBySegment($segment);
$routePrefix = $app->getRoutePrefixBySegment($segment);

/**
 * Mounting controller
 */
if (class_exists($className)) {
    $app->mount( ($app::hasLocale($app) ? '/{_locale}' : '') . $routePrefix , new $className());
} else {
    $e = new \Symfony\Component\Routing\Exception\RouteNotFoundException('Page not found', 404);
    $request = Request::createFromGlobals();
    if ($request->headers->has('content-type') && strtolower($request->headers->get('content-type')) == 'application/json') {
        return $app['output.json']($e);
    }
    return $app['output.render']('', $e);
}

