<?php

use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Silex\Provider\DoctrineServiceProvider;
use tfeiszt\silex\provider\OAuthServiceProvider;
use App\provider\OrmProvider;
use tfeiszt\silex\provider\OutputProvider;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new Sorien\Provider\PimpleDumpProvider());

/**
 * Register log service
 */
$app->register(new MonologServiceProvider(), []);
$app->extend('monolog', function($monolog, $app) {
    $monolog->pushHandler( new StreamHandler($app['monolog.config']['error_log_file'], Logger::ERROR) );
    return $monolog;
});
/**
 * Register debug log on "dev" and "staging"
 */
if ((ENVIRONMENT === 'dev') || (ENVIRONMENT === 'staging')){
    $app->extend('monolog', function($monolog, $app) {
        $monolog->pushHandler( new StreamHandler($app['monolog.config']['debug_log_file'], Logger::DEBUG) );
        return $monolog;
    });
}

/**
 * Template system definition.
 */
$app->register(new TwigServiceProvider(), array(
    'twig.options'		=> array(
       // 'cache'			=> isset($app['twig.config']['cache']) ? $app['twig.config']['cache'] : false,
        'strict_variables' => true
    ),
    'twig.path'		   => array( $app['twig.config']['template_path'] )
));

/**
 * Register external twig functions
 */
$app->extend('twig', function($twig, $app) {
    /*
    $twig->addFilter('json_decode', new \Twig_Filter_Function('json_decode'));
    $twig->addFilter('var_dump', new \Twig_Filter_Function('var_dump'));
    $twig->addFilter('get_object_vars', new \Twig_Filter_Function('get_object_vars'));
    $twig->addFilter('array_keys', new \Twig_Filter_Function('array_keys'));
    $twig->addFilter('current', new \Twig_Filter_Function('current'));*/
    return $twig;
});

/**
 * Output provider
 */
$app->register(new OutputProvider(), []);

/**
 * Profiler - Only for dev env
 */
if (ENVIRONMENT === 'dev') {
    $app->register(new WebProfilerServiceProvider(), array(
        'profiler.cache_dir' => $app['profiler.config']['cache_dir'],
        'profiler.debug_toolbar' => $app['profiler.config']['debug_toolbar'],
        'profiler.debug_redirects' => $app['profiler.config']['debug_redirects'],
    ));
}

/**
 * Register databases
 */
$app->register(new DoctrineServiceProvider, [
    'dbs.options' => $app['dbs.config']
]);

/**
 * Register World Sailing custom database ORM provider (Database name: isaf. Connection alias must be isaf in dbs.config)
 *
 */
$app->register(new OrmProvider());

/**
 * Validator
 */
$app->register(new Silex\Provider\ValidatorServiceProvider());

/**
 * OAuth service
 */
$app->register( new OAuthServiceProvider(), []);

/**
 * Localisation
 */
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));
$app['request_context']->setParameters(array('_locale' => $app['locale']));
$app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());

    $translator->addResource('yaml', __DIR__.'/../app/resource/locale/en.yaml', 'en');
    $translator->addResource('yaml', __DIR__.'/../app/resource/locale/hu.yaml', 'hu');

    return $translator;
});

/**
 * Second level cache
 */
$app->register(new Moust\Silex\Provider\CacheServiceProvider(), array(
    'cache.options' => array(
        'driver' => 'file',
        'cache_dir' => $app['cache.cache_dir']
    )
));

/**
 * Http cache
 * It's enabled if:
 * Config http_cache.enabled = true
 * AND
 * ENVIRONMENT is NOT dev
 * AND
 * Config debug = false
 */
$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => $app['http_cache.cache_dir'],
    'http_cache.options' => $app['http_cache.options'],
    'http_cache.esi'       => null,
));
$app['http_cache.strategy'] = $app->protect(function(Request $request, Response $response) {
    return $response
        ->setMaxAge(60)
        ->setPublic()
        ->setVary('Accept-Language')
        ->setETag(md5($response->getContent()))
        ->isNotModified($request);
});

return $app;

