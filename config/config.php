<?php
// Local
$app['locale'] = 'en';
$app['session.default_locale'] = $app['locale'];
$app['translator.messages'] = [
    'en' => PATH_LOCAL . '/en.yml',
];

// disable the debug mode by default
$app['debug'] = false;

// Cache
$app['cache.enabled'] = false;
$app['cache.cache_dir'] = PATH_CACHE . '/cache';

// Http cache
$app['http_cache.enabled'] = true;
$app['http_cache.cache_dir'] = PATH_CACHE . '/http';
$app['http_cache.options'] = [
    'allow_revalidate' => true,
    'allow_reload' => true,
];

//Log
$app['monolog.config'] = [
    'debug_log_file' => PATH_LOG . '/debug.log' ,
    'error_log_file' => PATH_LOG . '/error.log' ,
];

// Twig templates
$app['twig.config'] = [
    'cache' => PATH_CACHE . '/twig',
    'template_path' => PATH_TEMPLATES
];

// Profiler
$app['profiler.config'] = [
    'cache_dir' => PATH_CACHE . '/profiler',
    'debug_toolbar' => true,
    'debug_redirects' => false
];

require __DIR__ . '/db.php';

require __DIR__ . '/oauth.php';
