<?php

// Doctrine (dbs)
//It can be overridden in environment config file.
$app['dbs.config'] = [
    'default' => [
        'driver' => 'pdo_mysql',
        'charset'  => 'utf8',
        'host'     => '127.0.0.1',
        'dbname'   => 'skeleton',
        'user'     => 'root',
        'password' => 'root'
    ],
    'resource_2' => [
        'driver' => 'pdo_mysql',
        'charset'  => 'utf8',
        'host'     => '127.0.0.1',
        'dbname'   => 'resource_2_database_name',
        'user'     => 'root',
        'password' => 'root'
    ]
];
