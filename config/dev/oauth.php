<?php
// override OAuth parameters
$app['oauth.config'] = [
    'storage' => 'pdo',
    'connection' => [
        'dsn' => 'mysql:dbname=skeleton;host=localhost',
        'user' => 'root',
        'password' => 'root'
    ]
];
