<?php

require PATH_ROOT . '/config/config.php';

/**
 * Customs here
 */

require __DIR__ . '/db.php';

require __DIR__ . '/oauth.php';

// enable the debug mode
$app['debug'] = false;
