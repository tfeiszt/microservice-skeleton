<?php

// override DB parameters
$app['dbs.config'] = [
    'default' => [
        'driver' => 'pdo_mysql',
        'charset'  => 'utf8',
        'host'     => '127.0.0.1',
        'dbname'   => 'skeleton',
        'user'     => 'root',
        'password' => 'root'
    ]
];
