# Silex based skeleton framework 

### PHP library description
This framework can be used for building CRUD microservices.
  
### Installation

1. Via git
    ````
    git clone https://tfeiszt@bitbucket.org/tfeiszt/microservice-skeleton.git
    ````
2. Install packages:
    ````
    composer install
    ````

3. Create a database, and modify db name and credentials in following files:
    - app/config/ENVIRONMENT/db.php
    - app/config/ENVIRONMENT/oauth.php

4. Migration:
    ````
    SYMFONY_ENV=dev php ./bin/migration migration:migrate
    ````
### Endpoints
Requesting access token:
````
curl -u testclient:testpass http://skeleton.local/en/token/ -d 'grant_type=client_credentials'
````
Listing articles
````
GET http://skeleton.local/en/article/
````
Article entity
````
GET http://skeleton.local/en/article/{id}/
````
Create article entity
````
POST http://skeleton.local/en/article/ 
{"id": "10", "author": "Author Name", "text": Article text"}
````
Modify entity
````
PUT http://skeleton.local/en/article/{id}/ 
{"id": "10", "author": "Author Name", "text": Article text"}
````
Remove entity
````
DELETE http://skeleton.local/en/article/{id}/
````

### What's inside?

* `ValidatorServiceProvider` - Form validation
* `TwigServiceProvider` - Twig template engine
* `Symfony entity normalizer / serialze` - For JSON responses
* `WebProfilerServiceProvider` - Debugging
* `MonologServiceProvider` - Lgging
* `Oauth2` - Oauth2 protocol provider
* `Doctrine ORM` - ORM based data provider
* `Doctrine migration` - Migration tools 
* `Cache` - Http and second level cache 
* `Demo` - Models and controllers - how the thing works

See Silex [complete documentation](http://silex.sensiolabs.org/doc/1.3/)
 
### "Make your own kind of music"
 1. Fork master branch of this repository
 2. Develop your custom features
 3. Enjoy
 
### License

MIT
