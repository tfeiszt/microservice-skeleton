<?php
namespace App\resource\annotation\base\entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation;
use tfeiszt\silex\model\AbstractAnnotation;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use App\resource\annotation\base\Comment;
/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity
 */
class Article extends AbstractAnnotation
{
    public function __construct()
    {
        $this->relatives = new ArrayCollection();
    }

    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="article_id", type="integer", nullable=false)
     * @Annotation\Groups({"default", "article"})
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="article_author", type="string", length=80, nullable=true)
     * @Annotation\Groups({"default", "article"})
     */
    public $author;

    /**
     * @var string
     *
     * @ORM\Column(name="article_text", type="string", length=4000, nullable=true)
     * @Annotation\Groups({"default", "article"})
     */
    public $text;

    /**
     * @var string
     *
     * @ORM\Column(name="article_created", type="date", nullable=true)
     * @Annotation\Groups({"default", "article"})
     */
    public $created;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     * @Annotation\Groups({ "comments"})
     */
    public $comments;

    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [

        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'author' => [new Assert\Length(['max' => 80])]
            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getAllowedCriteria()
    {
        return [

        ];
    }
}
