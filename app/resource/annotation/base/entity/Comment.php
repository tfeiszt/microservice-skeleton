<?php
namespace App\resource\annotation\base\entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation;
use tfeiszt\silex\model\AbstractAnnotation;
use Symfony\Component\Validator\Constraints as Assert;
use App\resource\annotation\base\Article;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity
 */
class Comment extends AbstractAnnotation
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="comment_id", type="integer", nullable=false)
     * @Annotation\Groups({"default", "comment", "comments"})
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_text", type="string", length=4000, nullable=true)
     * @Annotation\Groups({"default", "comment", "comments"})
     */
    public $text;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="comment_article", referencedColumnName="article_id")
     * @Annotation\Groups({"comment"})
     */
    public $article;


    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [

        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'name' => [new Assert\Length(['max' => 3])]
            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getAllowedCriteria()
    {
        return [

        ];
    }
}
