<?php

namespace App\model\base;

use tfeiszt\silex\model\ORMModelInterface;
use tfeiszt\silex\model\ORMTrait;

/**
 * Class Article
 * @package App\model\base
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Article extends \App\resource\annotation\base\entity\Article implements ORMModelInterface
{
    use ORMTrait;

    protected static $_annotation = 'App\resource\annotation\base\entity\Article';
    protected static $_db = 'base';
}