<?php

namespace App\model\base;

use tfeiszt\silex\model\ORMModelInterface;
use tfeiszt\silex\model\ORMTrait;

/**
 * Class Comment
 * @package App\model\base
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Comment extends \App\resource\annotation\base\entity\Comment implements ORMModelInterface
{
    use ORMTrait;

    protected static $_annotation = 'App\resource\annotation\base\entity\Comment';
    protected static $_db = 'base';
}