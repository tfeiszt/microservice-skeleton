<?php
namespace App\command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class SampleCommand
 * @package App\command
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class SampleCommand extends Command
{
    protected function configure()
    {
        $this->setName('app:sample');
        $this->addArgument('env', InputArgument::REQUIRED, 'Environment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Sample command',
            '==============',
            '',
        ]);

        // the value returned by someMethod() can be an iterator (https://secure.php.net/iterator)
        // that generates and returns the messages with the 'yield' PHP keyword
        // $output->writeln($this->someMethod());

        // outputs a message followed by a "\n"
        $output->writeln('Done!');
    }
}
