<?php

namespace App\migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180830203838 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE article (article_id INT AUTO_INCREMENT NOT NULL, article_author VARCHAR(80) DEFAULT NULL, article_text VARCHAR(4000) DEFAULT NULL, article_created DATE DEFAULT NULL, PRIMARY KEY(article_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (comment_id INT AUTO_INCREMENT NOT NULL, comment_article INT DEFAULT NULL, comment_text VARCHAR(4000) DEFAULT NULL, INDEX IDX_9474526CF1496C76 (comment_article), PRIMARY KEY(comment_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF1496C76 FOREIGN KEY (comment_article) REFERENCES article (article_id)');

        $this->addSql('CREATE TABLE oauth_clients (
              client_id             VARCHAR(80)   NOT NULL,
              client_secret         VARCHAR(80),
              redirect_uri          VARCHAR(2000),
              grant_types           VARCHAR(80),
              scope                 VARCHAR(4000),
              user_id               VARCHAR(80),
              PRIMARY KEY (client_id));
        ');
        $this->addSql('CREATE TABLE oauth_access_tokens (
              access_token         VARCHAR(40)    NOT NULL,
              client_id            VARCHAR(80)    NOT NULL,
              user_id              VARCHAR(80),
              expires              TIMESTAMP      NOT NULL,
              scope                VARCHAR(4000),
              PRIMARY KEY (access_token));
        ');
        $this->addSql('CREATE TABLE oauth_authorization_codes (
              authorization_code  VARCHAR(40)     NOT NULL,
              client_id           VARCHAR(80)     NOT NULL,
              user_id             VARCHAR(80),
              redirect_uri        VARCHAR(2000),
              expires             TIMESTAMP       NOT NULL,
              scope               VARCHAR(4000),
              id_token            VARCHAR(1000),
              PRIMARY KEY (authorization_code));
        ');
        $this->addSql('CREATE TABLE oauth_refresh_tokens (
              refresh_token       VARCHAR(40)     NOT NULL,
              client_id           VARCHAR(80)     NOT NULL,
              user_id             VARCHAR(80),
              expires             TIMESTAMP       NOT NULL,
              scope               VARCHAR(4000),
              PRIMARY KEY (refresh_token));
        ');
        $this->addSql('CREATE TABLE oauth_users (
              username            VARCHAR(80),
              password            VARCHAR(80),
              first_name          VARCHAR(80),
              last_name           VARCHAR(80),
              email               VARCHAR(80),
              email_verified      BOOLEAN,
              scope               VARCHAR(4000),
              PRIMARY KEY (username));
        ');
        $this->addSql('CREATE TABLE oauth_scopes (
              scope               VARCHAR(80)     NOT NULL,
              is_default          BOOLEAN,
              PRIMARY KEY (scope));
        ');
        $this->addSql('CREATE TABLE oauth_jwt (
              client_id           VARCHAR(80)     NOT NULL,
              subject             VARCHAR(80),
              public_key          VARCHAR(2000)   NOT NULL);
        ');


        $this->addSql("
            INSERT INTO article VALUES (NULL, 'John Doe', 'Article one text', NOW());
            INSERT INTO article VALUES (NULL, 'Thomas Spiers', 'Article two text', NOW());
            INSERT INTO article VALUES (NULL, 'Anita Fram', 'Article three text', NOW());
            INSERT INTO article VALUES (NULL, 'Kevin Hetzner', 'Article four text', NOW());
            
            INSERT INTO comment VALUES (NULL, 1, 'Brilliant article');
            INSERT INTO comment VALUES (NULL, 1, 'Not bad');
            INSERT INTO comment VALUES (NULL, 2, 'Fair enough');
            
            INSERT INTO oauth_clients (client_id, client_secret, user_id) VALUES ('testclient', 'testpass', 1)"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CF1496C76');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE oauth_clients');
        $this->addSql('DROP TABLE oauth_access_tokens');
        $this->addSql('DROP TABLE oauth_authorization_codes');
        $this->addSql('DROP TABLE oauth_refresh_tokens');
        $this->addSql('DROP TABLE oauth_users');
        $this->addSql('DROP TABLE oauth_scopes');
        $this->addSql('DROP TABLE oauth_jwt');

    }
}
