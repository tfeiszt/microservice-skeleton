<?php

namespace App\provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;

/**
 * Class OrmProvider
 * @package App\provider
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class OrmProvider implements ServiceProviderInterface
{
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        /**
         * Register Doctrine ORM for data tables
         */
        $app->register(new DoctrineOrmServiceProvider, [
            'orm.ems.options' => [
                'base' => [
                    'connection' => "default",
                    'auto_generate_proxy_classes' => "%kernel.debug%",
                    'auto_mapping' => false,
                    'mappings' => [
                        [
                            'type' => "annotation",
                            'namespace' => "App\\resource\\annotation\\base\\entity\\Article",
                            'path' => __DIR__ . "/../resource/annotation/base/entity",
                            'use_simple_annotation_reader' => false //DO NOT CHANGE OR DELETE IT! If this is true OR missing then you must use @Entity on Doctrine entity rather than @ORM\Entity
                        ],
                        [
                            'type' => "annotation",
                            'namespace' => "App\\resource\\annotation\\base\\entity\\Comment",
                            'path' => __DIR__ . "/../resource/annotation/base/entity",
                            'use_simple_annotation_reader' => false //DO NOT CHANGE OR DELETE IT! If this is true OR missing then you must use @Entity on Doctrine entity rather than @ORM\Entity
                        ]
                    ],
                ]
            ]
        ]);

        return;
    }
}
