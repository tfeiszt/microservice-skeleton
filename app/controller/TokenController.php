<?php
namespace App\controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use tfeiszt\helper\Helper;
use tfeiszt\silex\controller\AbstractController;

/**
 * Class TokenController
 * @package App\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class TokenController extends AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app )
    {
        $controller = parent::connect($app);
        /*
         * Register additional methods
         */
        $controller->post("/", array( $this, 'token' ) )->bind( Helper::getClassShortName(static::class) . '_token' );
        return $controller;
    }

    /**
     * @param Application $app
     * @return void
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function token(Application $app)
    {
        $app['service.oauth.server']->handleTokenRequest(\OAuth2\Request::createFromGlobals())->send();
        die();
    }
}
