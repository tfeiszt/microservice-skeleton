<?php
namespace App\controller;

use OAuth2\Request;
use Silex\Application;
use tfeiszt\silex\controller\AbstractController;
use Silex\Api\ControllerProviderInterface;
use tfeiszt\helper\Helper;
use tfeiszt\silex\model\Record;

/**
 * Class IndexController
 * @package App\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class IndexController extends AbstractController  implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app )
    {
        $controller = parent::connect($app);
        $controller->get("/" , array( $this, 'index' ) )->bind( Helper::getClassShortName(static::class) . '_welcome_page' );
        return $controller;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function index(Application $app)
    {
        return $app['output.render']('index/index');
    }
}
