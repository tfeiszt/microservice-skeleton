<?php
namespace App\controller;

use Silex\Application;
use tfeiszt\silex\controller\AbstractResourceController;
use Silex\Api\ControllerProviderInterface;

/**
 * Class ArticleController
 * @package App\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class ArticleController extends AbstractResourceController implements ControllerProviderInterface
{
    protected static $_modelClass = '\App\model\base\Article';
    protected static $_resourceGroups = [
        'retrieve' => ['article', 'comments']
    ];

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app )
    {
        $controller = parent::connect($app);
        /*
         * Register additional methods
         */
        return $controller;
    }
}
